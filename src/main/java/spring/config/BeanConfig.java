package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import sprint.bean.BeanB;
import sprint.bean.BeanC;
import sprint.bean.BeanD;

@Configuration
public class BeanConfig {

    @Bean("beanB")
    @DependsOn(value = {"beanD", "beanC"})
    public BeanB getBeanB(){
        BeanB beanB = new BeanB();
        beanB.satName("Vasia");
        beanB.satValue(25);
        return beanB;
    }

    @Bean("beanC")
    public BeanC getBeanC(){
        BeanC beanC = new BeanC();
        beanC.satName("Ura");
        beanC.satValue(30);
        return beanC;
    }

    @Bean("beanD")
    public BeanD getBeanD(){
        BeanD beanD = new BeanD();
        beanD.satName("Ira");
        beanD.satValue(20);
        return beanD;
    }
}
