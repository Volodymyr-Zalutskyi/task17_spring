package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sprint.bean.BeanE;

@Configuration
public class BeanEConfig {
    @Bean
    public BeanE getBeanE(){
        BeanE beanE = new BeanE();
        beanE.satName("Oleksandr");
        beanE.satValue(40);
        return beanE;
    }
}
