package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import sprint.bean.BeanA;

@Configuration
@Import(BeanConfig.class)
public class BeanAConfig {
    @Bean
    public BeanA getBeanA(){
        return new BeanA();
    }
}
