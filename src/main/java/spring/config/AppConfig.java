package spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import sprint.bean.BeanB;

@Configuration
@PropertySource("my.properties")
public class AppConfig {
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private int value;

    @Bean
    public BeanB getBeanB() {
        return new BeanB(name, value);
    }

}
