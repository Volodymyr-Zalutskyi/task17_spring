package sprint.bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE {
    private String name;
    private int value;

    public void satName(String name) {
        this.name = name;
    }

    public void satValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @PostConstruct
    public void init() throws Exception {
        System.out.println("- - - initializing beanE using initializingBean");
        System.out.println("name: " + name + ", value: " + value);

    }

    @PreDestroy
    public void destroy() {
        System.out.println("- - - destroying beanE using DisposableBean");

    }
}
