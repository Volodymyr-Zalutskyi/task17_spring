package sprint.bean;

public class BeanC {
    private String name;
    private int value;

    public BeanC() {

    }

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void satName(String name) {
        this.name = name;
    }

    public void satValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
