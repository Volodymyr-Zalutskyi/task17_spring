package sprint.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;


public class BeanA implements InitializingBean, DisposableBean {
    private String name;
    private int value;

    public BeanA() {

    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void satName(String name) {
        this.name = name;
    }

    public void satValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("- - - initializing beanA using initializingBean");
        System.out.println("name: " + name + ", value: " + value);

    }
    @Override
    public void destroy() {
        System.out.println("- - - destroying beanA using DisposableBean");

    }
}
