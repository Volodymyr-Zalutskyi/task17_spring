package sprint.bean;

public class BeanD {
    private String name;
    private int value;

    public BeanD() {

    }

    public BeanD(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void satName(String name) {
        this.name = name;
    }

    public void satValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
