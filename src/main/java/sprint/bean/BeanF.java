package sprint.bean;

public class BeanF {
    private String name;
    private int value;

    public void satName(String name) {
        this.name = name;
    }

    public void satValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
