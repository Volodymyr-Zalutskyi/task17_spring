package sprint.bean;

public class BeanB {
    private String name;
    private int value;

    public BeanB() {

    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void satName(String name) {
        this.name = name;
    }

    public void satValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
