import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.config.AppConfig;
import spring.config.BeanAConfig;
import spring.config.BeanConfig;
import spring.config.BeanEConfig;
import sprint.bean.*;

public class Application {
    public static void main(String[] args) {
        //ApplicationContext applicationContext = new AnnotationConfigApplicationContext(BeanAConfig.class);
        //ApplicationContext applicationContext = new AnnotationConfigApplicationContext(BeanConfig.class);
//        BeanA beanA = applicationContext.getBean(BeanA.class);
//        beanA.satName("me");
//        beanA.satValue(35);
//        System.out.println(beanA);
//        BeanB beanB = applicationContext.getBean(BeanB.class);
//        System.out.println(beanB);
//        BeanC beanC = applicationContext.getBean(BeanC.class);
//        System.out.println(beanC);
//        BeanD beanD = applicationContext.getBean(BeanD.class);
//        System.out.println(beanD);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanEConfig.class);
//        BeanA beanA = context.getBean(BeanA.class);
//        beanA.satName("me");
//        beanA.satValue(35);

        BeanE beanE = context.getBean(BeanE.class);

        context.close();

    }
}
